package org.gopas.javadb.demo.data.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {

    private static volatile Connection connection;

    //jdbc:postgresql://host:port/database
    public static Connection getConnection() {
        if (connection == null) {
            synchronized (DbConnection.class) {
                if (connection == null) {
                    try {
                        // nikdy nepouzivat DriverManager ... vzdy preferovat DataSource
                        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/java-training"
                                , "postgres",
                                "postgres");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return connection;
    }
}
