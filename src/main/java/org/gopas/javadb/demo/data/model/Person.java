package org.gopas.javadb.demo.data.model;

import java.time.LocalDate;

public class Person {
    private Long idPerson;
    private LocalDate birthday;
    private String email;
    private int age;

    public Person() {
    }

    public Long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Long idPerson) {
        this.idPerson = idPerson;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "idPerson=" + idPerson +
                ", birthday=" + birthday +
                ", email='" + email + '\'' +
                ", age=" + age +
                '}';
    }
}
