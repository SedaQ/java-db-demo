package org.gopas.javadb.demo.data.repository;

import org.gopas.javadb.demo.data.config.DbConnection;
import org.gopas.javadb.demo.data.model.Person;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PersonRepository {

    // INSERT,
    // UPDATE,
    // DELETE,
    // A PRIDAT SELECT, ktery vrati nejaka jina data nez aktualni objekt Person (bude nutne vytvorit novy objekt)
    // SELECT --- ktery vrati Person, Address, Contact... ci neco dle vasi vule

    // logic
    private JdbcTemplate jdbcTemplate;

    public PersonRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Person> findAllJdbcTemplate() {
        return jdbcTemplate.query("SELECT * FROM person",
                BeanPropertyRowMapper.newInstance(Person.class));
    }

    public List<Person> findAll() {
        List<Person> persons = new ArrayList<>();
        Connection connection = DbConnection.getConnection();
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM person")
        ) {
            while (resultSet.next()) {
                Person person = new Person();
                person.setAge(resultSet.getInt("age"));
                person.setBirthday(resultSet.getDate("birthday").toLocalDate());
                person.setEmail(resultSet.getString("email"));
                person.setIdPerson(resultSet.getLong("id_person"));
                persons.add(person);
            }
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return persons;
    }
}
