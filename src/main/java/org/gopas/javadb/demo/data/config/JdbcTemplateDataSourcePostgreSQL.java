package org.gopas.javadb.demo.data.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.sql.SQLException;

public class JdbcTemplateDataSourcePostgreSQL {

    private static HikariConfig config = new HikariConfig();
    private static volatile HikariDataSource ds;

    static {
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/java-training");
        config.setUsername("postgres");
        config.setPassword("postgres");
        ds = new HikariDataSource(config);
    }

    private JdbcTemplateDataSourcePostgreSQL() {
    }

    public static synchronized DataSource getDataSource() throws SQLException {
        return ds;
    }
}
