package org.gopas.javadb.demo;

import org.gopas.javadb.demo.data.config.JdbcTemplateDataSourcePostgreSQL;
import org.gopas.javadb.demo.data.model.Person;
import org.gopas.javadb.demo.data.repository.PersonRepository;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;
import java.util.List;

public class App {

    public static void main(String[] args) throws SQLException {
        JdbcTemplate jdbcTemplatePostgreSQL = new JdbcTemplate(JdbcTemplateDataSourcePostgreSQL.getDataSource());
        PersonRepository personRepository = new PersonRepository(jdbcTemplatePostgreSQL);

        List<Person> persons = personRepository.findAllJdbcTemplate();
        for (Person person : persons) {
            System.out.println(person);
        }



//        List<Person> persons = personRepository.findAll();
//        for (Person person : persons) {
//            System.out.println(person);
//        }
    }
}
